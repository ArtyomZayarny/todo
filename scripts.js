'use strict';

var cardForm    = document.getElementById('cardForm');
var createBtn   = document.querySelector('[data-create]');
var titleField  = document.getElementById('title');
var textField   = document.getElementById('text');
var fields      = cardForm.querySelectorAll('input[type="text"], input[type="checkbox"]');
var errorBlocks = cardForm.querySelectorAll('span.error');


function showError(fields){
            fields.forEach(function(input){
                if (input.nextElementSibling) {
                    if (input.value === '') {
                    input.nextElementSibling.textContent = 'Заполните это поле';
                    } else {
                    input.nextElementSibling.textContent = '';
                    }
                }  
            });
        }
function clearError(errorBlocks){
        errorBlocks.forEach(function(errorMessage){
            errorMessage.textContent = '';
        });
    }
function clearForm(el){
        el.forEach(function(input){
            input.value = '';
            if (input.checked == true) {
                input.checked = false;
            }
        });
    }
function getData (){
    let data = {};
    data['title'] = titleField.value;
    data['text']  = textField.value;
    data['important'] = important.checked;
    return data;
}
 createBtn.addEventListener('click', function(e) {
     e.preventDefault();
     //validate form
     if (titleField.value !='' && textField.value != '') {
        clearError(errorBlocks);
        var data = getData();
        var card =  new Card(data);
        card.create();
        clearForm(fields);
    }else{
        // find empty field and show error message
        showError(fields)
        }
     
 });

 function Card(data) {
   var self = this;
   this.title         = data.title;
   this.text          = data.text;
   this.important     = data.important;
   this.cardBox ;
   this.cardTitle;
   this.cardText;
   this.cardImportant; 
   this.editBtn;
   this.deleteBtn;
   
   this.editTitle;
   this.editText;
   this.cardContainer = document.getElementById('cardsbox');

this._fillNodes = function (){
    //Fill card info
    this.cardTitle.textContent = this.title;
    this.cardText.textContent  = this.text;
    this.cardImportant.checked = this.important;
}
this._fillCard = function () {
     //Paste elements into card
     this.cardBox.appendChild(this.cardTitle);
     this.cardBox.appendChild(this.editTitle);
     this.cardBox.appendChild(this.cardText);
     this.cardBox.appendChild(this.editText);
     this.cardBox.appendChild(this.cardImportant);
     this.cardBox.appendChild(this.editBtn);
     this.cardBox.appendChild(this.deleteBtn);

    //Paste card into container
     this.cardContainer.appendChild(this.cardBox);
}
this._createNodes = function() {
    //Create <div class='card-js>
    this.cardBox = document.createElement('div');
    this.cardBox.classList.add('card-js');
    //Create h1
    this.cardTitle = document.createElement('h1');
    //Create p
    this.cardText  = document.createElement('p');
    //Create <input type="checkbox"/>
    this.cardImportant = document.createElement('input');
    this.cardImportant.type = 'checkbox';
    this.cardImportant.setAttribute("disabled", "disabled");
    
    //Creating the editable fields by default display none
    this.editTitle = document.createElement('input');
    this.editTitle.setAttribute('data-edit','title');
    this.editTitle.style.display = 'none';

    this.editText = document.createElement('input');
    this.editText.setAttribute('data-edit','text');
    this.editText.style.display = 'none';

    //Create Edit button
    this.editBtn = document.createElement('button');
    this.editBtn.setAttribute('data-action','EDIT');
    this.editBtn.textContent = 'EDIT';
    
    //Create Delete button
    this.deleteBtn = document.createElement('button');
    this.deleteBtn.setAttribute('data-action','DELETE');
    this.deleteBtn.textContent = 'DELETE';
    
}
this._cardNodes = function (card) {
    //return Nodelist of input
    let cardNodes = {
        title          : card.querySelector('h1'),
        text           : card.querySelector('p'),
        inputTitle     : card.querySelector('input[data-edit="title"]'),
        inputText      : card.querySelector('input[data-edit="text"]'),
        inputImportant : card.querySelector('input[type="checkbox"]'),
        btn        : card.querySelector('button[data-action]')
    }

    return cardNodes;
}
 this._editCard = function (card) {
     let cardObj = self._cardNodes(card);
    //fetch old data
    let oldData = {
        titleVal: cardObj.title.textContent,
        textVal:cardObj.text.textContent,
        iputImportantValue: cardObj.inputImportant.checked
    }
    //hide old nodes
    cardObj.title.style.display = 'none';
    cardObj.text.style.display = 'none';
    
     //paste value into editable field
     cardObj.inputTitle.style.display = 'block';
     cardObj.inputText.style.display   = 'block';
     cardObj.inputImportant.disabled = false;

     cardObj.inputTitle.value = oldData.titleVal;
     cardObj.inputText.value  = oldData.textVal;
     
    //change Edit button action and text 
     cardObj.btn.dataset.action = 'SAVE'; 
     cardObj.btn.textContent = 'SAVE';
 }
 this._updateCard = function (card) {
    let cardObj = self._cardNodes(card);
     
    //get new values from input
    let newTitle = cardObj.inputTitle.value;
    let newText  = cardObj.inputText.value;
    let newImportant = cardObj.inputImportant.checked;
  
    //hide inputs
    cardObj.inputTitle.style.display = 'none';
    cardObj.inputText.style.display = 'none';

    //paste new value
    cardObj.title.textContent = newTitle;
    cardObj.text.textContent = newText;
    cardObj.inputImportant.checked = newImportant;
    cardObj.inputImportant.disabled = true;

    //show update title and text
    cardObj.title.style.display = 'block';
    cardObj.text.style.display = 'block';

     //Change action and text
    cardObj.btn.dataset.action = 'EDIT';
    cardObj.btn.textContent    = 'EDIT';
     
 }
this._attachEvent  = function () {
        self.cardBox.addEventListener('click', function (e) {
            let action = e.target.dataset.action;
             switch(action) {
                 case 'EDIT': 
                            self._editCard(self.cardBox);
                        break;
                case 'DELETE' :
                            self.cardContainer.removeChild(self.cardBox);
                            break;
                case 'SAVE' :
                    self._updateCard(self.cardBox)
                    break;
                       
             }

        });     
    }
     return{
         create: function () {
             self._createNodes();      
             self._fillNodes();
             self._fillCard();
             self._attachEvent();
         }
     }
 }


    
    



